﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using JugglingStoreProject.ClassModels;
using JugglingStoreProject.DataAccess;


namespace JugglingStoreProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static User currentCustomer;

        public List<User> customers = new CustomerDataAccess().GetAllCustomers();
        public MainWindow()
        {
            InitializeComponent();


            var loggedInCustomer = LoginDetails.GetInstnace();
            var currentCustomerId = customers.Find(c => c.UserName == loggedInCustomer.UserName && c.Password == loggedInCustomer.Password).UserId;

            currentCustomer = new CustomerDataAccess().GetCustomerById(currentCustomerId);

            if (currentCustomer.IsAdmin == true)
            {
                List<Order> allOrders = OrderDataAccess.GetAllOrders();

                LoadAllOrdersBtn.Visibility = Visibility.Visible;

                totalUserssCount.Text = $"Total Users: {customers.Count()}";
                totalOrdersCount.Text = $"Total Orders: {allOrders.Count()}";
            }
            else
            {
                LoadAllOrdersBtn.Visibility = Visibility.Hidden;
                totalUserssCount.Text = "";
                totalOrdersCount.Text = "";
            }

            DataContext = this;
        }


        private void LoadPersonalOrders_Click_1(object sender, RoutedEventArgs e)
        {

            var PersonalOrders = new OrderDataAccess().GetPersonalOrders(currentCustomer.UserId);
            DataGrid1.ItemsSource = PersonalOrders;

        }


        private void NewOrder_Click(object sender, RoutedEventArgs e)
        {

            Window1 CreateOrderWindow = new Window1();
            CreateOrderWindow.ShowDialog();
        }

        private void viewOrderById_Click(object sender, RoutedEventArgs e)
        {
            int value;
            if (OrderIdTextBox.Text == "")
            {
                MessageBox.Show("Please enter order id !");
            }

            else if (int.TryParse(OrderIdTextBox.Text, out value) == false)
            {
                MessageBox.Show("input id not valid !");
            }

            else if (currentCustomer.IsAdmin == false && int.TryParse(OrderIdTextBox.Text, out value) == true && currentCustomer.Orders.Any(o => o.OrderId == value) == false)
            {
                MessageBox.Show("Order not Found !");
            }

            else
            {
                var selectedOrder = OrderDataAccess.GetOrderByID(int.Parse(OrderIdTextBox.Text));

                if (selectedOrder == null)
                {
                    MessageBox.Show("Order not Found !");
                }

                else
                {
                    string itemsNames = "";
                    double totalPrice = 0;
                    foreach (var item in selectedOrder.Items)
                    {
                        itemsNames += item.Quantity + " " + item.ItemName + ",";

                        totalPrice += item.Price * item.Quantity;
                        if (currentCustomer.IsAdmin == true)
                        {
                            totalPrice = totalPrice * 0.9;
                        }
                    }
                    MessageBox.Show($"ORDER DETAILS:\n" +
                        $"order id: {selectedOrder.OrderId}\n" +
                        $"order date of order: {selectedOrder.OrderDate}\n" +
                        $"order date of shipment: { selectedOrder.ShipmentDate}\n" +
                        $"order items: {itemsNames}\n" +
                        $"order total price: {totalPrice}");
                }   
            }
        }

        private void filterOrdersByDates_Click(object sender, RoutedEventArgs e)
        {
            if (startDatePicker.Text == "" || endDatePicker.Text == "")
            {
                MessageBox.Show("Please pick both start and end date !");
            }

            else
            {

                int customerId = currentCustomer.UserId;

                DateTime StartDate = startDatePicker.SelectedDate.Value;

                DateTime endDate = endDatePicker.SelectedDate.Value;

                DataGrid1.ItemsSource = OrderDataAccess.GetOrdersBetweenDates(customerId, StartDate, endDate);
                CollectionViewSource.GetDefaultView(DataGrid1.ItemsSource).Refresh();
            }

        }

        private void DataGrid1_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "TotalPrice")
                e.Column.Visibility = System.Windows.Visibility.Collapsed;
            if (e.PropertyName == "Items")
                e.Column.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void quitMain_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LoadAllOrdersBtn_Click(object sender, RoutedEventArgs e)
        {

            DataGrid1.ItemsSource = OrderDataAccess.GetAllOrders();
            CollectionViewSource.GetDefaultView(DataGrid1.ItemsSource).Refresh();
        }

        private void DeleteOrderBtn_Click(object sender, RoutedEventArgs e)
        {
            int value;
            if (OrderIdForDeleteTextBox.Text == "")
            {
                MessageBox.Show("Please enter order id for deletion !");
            }

            else if (int.TryParse(OrderIdForDeleteTextBox.Text, out value) == false)
            {
                MessageBox.Show("input id not valid !");
            }

            else if (currentCustomer.IsAdmin == false && currentCustomer.Orders.Any(o => o.OrderId == value) == false)
            {
                MessageBox.Show("Order not Found !");
            }

            else
            {
                var selectedOrder = OrderDataAccess.GetOrderByID(int.Parse(OrderIdForDeleteTextBox.Text));

                if (selectedOrder == null)
                {
                    MessageBox.Show("Order not Found !");
                }
                else
                {
                    OrderDataAccess.DeleteOrder(int.Parse(OrderIdForDeleteTextBox.Text));
                    if (currentCustomer.IsAdmin == true)
                    {
                        DataGrid1.ItemsSource = OrderDataAccess.GetAllOrders();
                    }
                    else
                    {
                        DataGrid1.ItemsSource = new OrderDataAccess().GetPersonalOrders(currentCustomer.UserId);
                    }
                    
                    CollectionViewSource.GetDefaultView(DataGrid1.ItemsSource).Refresh();

                    if (currentCustomer.IsAdmin == true)
                    {
                        List<Order> allOrders = OrderDataAccess.GetAllOrders();

                        LoadAllOrdersBtn.Visibility = Visibility.Visible;

                        totalUserssCount.Text = $"Total Users: {customers.Count()}";
                        totalOrdersCount.Text = $"Total Orders: {allOrders.Count()}";
                    }
                    else
                    {
                        LoadAllOrdersBtn.Visibility = Visibility.Hidden;
                        totalUserssCount.Text = "";
                        totalOrdersCount.Text = "";
                    }
                }   
            }
        }

        private void EditOrderBtn_Click(object sender, RoutedEventArgs e)
        {
            int value;
            if (OrderIdEditTxtBox.Text == "")
            {
                MessageBox.Show("please enter order id for edit !");
            }

            else if (int.TryParse(OrderIdEditTxtBox.Text, out value) == false)
            {
                MessageBox.Show("input id not valid !");
            }

            else if (currentCustomer.IsAdmin == false && currentCustomer.Orders.Any(o => o.OrderId == value) == false)
            {
                MessageBox.Show("Order not Found !");
            }

            else 
            {
                Order currentOrder = OrderDataAccess.GetOrderByID(int.Parse(OrderIdEditTxtBox.Text));


                if (currentOrder == null)
                {
                    MessageBox.Show("Order not Found !");
                }
                else
                {
                    EditOrderWindow CreateOrderWindow = new EditOrderWindow(currentOrder);
                    CreateOrderWindow.ShowDialog();
                }  
            }
        }
    }
}
