﻿using JugglingStoreProject.ClassModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;



namespace JugglingStoreProject.DataAccess
{
    public class CustomerDataAccess : ICustomer
    {
        // Customer data Methods:

        public List<User> GetAllCustomers()
        {
            using (var dbContext = new JugglingStoreContext())
            {
                var users = dbContext.Users.ToList();
                return users;
            }
        }

        public bool AddCustomer(string userName, string password, string firstName, string lastName, string address, bool isVip)
        {
            User user = new User()
            {
                UserName = userName,
                FirstName = firstName,
                LastName = lastName,
                Password = password,
                Address = address,
                IsAdmin = isVip
            };

            using (var dbContext = new JugglingStoreContext())
            {
                if (dbContext.Users.Contains(user) || dbContext.Users.Any(c => c.UserName == userName) || dbContext.Users.Any(c => c.Password == password))
                {
                    return false;
                }
                else
                {
                    dbContext.Users.Add(user);
                    dbContext.SaveChanges();
                    return true;
                }
            }
        }

        public User GetCustomerById(int id)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                var customer = dbContext.Users.Include("Orders").FirstOrDefault(x => x.UserId == id);
                return customer;
            }
        }

        public void PopulateCustomerTable() // method to populate Db with records.
        {
            AddCustomer("IdoNavot303", "12312", "Ido", "Navot", "Haifa, Neve shaanan, Pinsker 27", true);
            AddCustomer("AviArnavi999", "100452", "Avi", "Levy", "Beer sheva, rabin 72", false);
            AddCustomer("Dandi88", "972310", "Daniel", "Cohen", "Tel aviv, Rothchild 80", false);
            AddCustomer("YoavDude", "61259", "Yoav", "Pintel", "Sderot, Hanavi 14", true);
            AddCustomer("Eliko245", "114425", "Eli", "Laban", "Haifa, Hadar 66", false);
            AddCustomer("SuperNiro", "762390", "Nir", "Barak", "Yokneam, Moshe dayan 25", false);
        }


    }
    public interface ICustomer
    {
        abstract List<User> GetAllCustomers();
        abstract bool AddCustomer(string userName, string password, string firstName, string lastName, string address, bool isVip);
        abstract User GetCustomerById(int id);
    }
}

