﻿using System;
using JugglingStoreProject.ClassModels;
using Microsoft.EntityFrameworkCore;


#nullable disable

namespace JugglingStoreProject.DataAccess
{
    public partial class JugglingStoreContext : DbContext // connection class to the db.
    {
        public DbSet<User> Users { get; set; } //Customer table.
        public DbSet<Order> Orders { get; set; } // Orders table.
        public DbSet<Item> Items { get; set; } // items Table.
        public DbSet<ItemOrder> ItemOrder { get; set; }
        public JugglingStoreContext()
        {

        }

        public JugglingStoreContext(DbContextOptions<JugglingStoreContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server= LAPTOP-9192FB3J\\SQLEXPRESS ;Database= JugglingStore;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Hebrew_CI_AS");

            modelBuilder.Entity<ItemOrder>().HasKey(l => new { l.OrderId, l.ItemId });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
