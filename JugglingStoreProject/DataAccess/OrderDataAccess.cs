﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using JugglingStoreProject.ClassModels;
using Microsoft.EntityFrameworkCore;

namespace JugglingStoreProject.DataAccess
{

    public class OrderDataAccess // Orders data methods:
    {

        public static void AddOrder(Order order)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                dbContext.Add(order);
                dbContext.SaveChanges();
            }
        }

        public static void DeleteOrder(int id)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Order order = dbContext.Orders.Find(id);
                dbContext.Remove(order);
                dbContext.SaveChanges();
            }
        }



        public List<Order> GetPersonalOrders(int id)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                var personalOrders = dbContext.Orders.Where(o => o.UserId == id).ToList();
                return personalOrders;
            }
        }
        public static List<Order> GetAllOrders()
        {
            var orders = new List<Order>();
            using (var dbContext = new JugglingStoreContext())
            {
                orders = dbContext.Orders.ToList();
            }
            return orders;
        }
        public static Order GetOrderByID(int id)
        {
            var order = new Order();
            var items = new List<Item>();
            using (var dbContext = new JugglingStoreContext())
            {
                order = dbContext.Orders.Include("Items").FirstOrDefault(o => o.OrderId == id);
            }
            return order;
        }

        public static List<Order> GetOrdersBetweenDates(int customerId, DateTime start, DateTime end)
        {
            var orders = new List<Order>();
            using (var dbContext = new JugglingStoreContext())
            {
                orders = dbContext.Orders.Where(o => (o.OrderDate >= start) && (o.OrderDate <= end) && (o.UserId == customerId)).ToList();
            }
            return orders;
        }

        public static void PopulateOrderTable() // method to poulate the Db with Orders records.
        {
            var initOrders = new List<Order>();
            using (var dbContext = new JugglingStoreContext())
            {
                Item staff = dbContext.Items.FirstOrDefault(i => i.ItemName == "staff");
                Item jugglingBalls = dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling balls");
                Item poi = dbContext.Items.FirstOrDefault(i => i.ItemName == "poi");
                Item flowerSticks = dbContext.Items.FirstOrDefault(i => i.ItemName == "flower sticks");
                Item jugglingSword = dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling sword");
                Item ropeDart = dbContext.Items.FirstOrDefault(i => i.ItemName == "rope dart");

                var customers = new CustomerDataAccess().GetAllCustomers();

                if (dbContext.Orders.Count() == 0)
                {


                    // change date foramt on all orders.
                    Order order1 = new Order()
                    {
                        UserId = customers[0].UserId,
                        OrderDate = DateTime.ParseExact("2021/06/01", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/06/06", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder() { Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName }
                        }
                    };

                    Order order2 = new Order()
                    {
                        UserId = customers[0].UserId,
                        OrderDate = DateTime.ParseExact("2021/05/11", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/05/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 3, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 2, ItemId = poi.ItemId, ItemName = poi.ItemName  }
                        }
                    };

                    Order order3 = new Order()
                    {
                        UserId = customers[0].UserId,
                        OrderDate = DateTime.ParseExact("2021/08/11", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 2 , ItemId = poi.ItemId, ItemName = poi.ItemName },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  }
                        }
                    };

                    Order order4 = new Order()
                    {
                        UserId = customers[0].UserId,
                        OrderDate = DateTime.ParseExact("2021/08/10", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/15", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {

                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 2, ItemId = poi.ItemId, ItemName = poi.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  }
                        }
                    };

                    Order order5 = new Order()
                    {
                        UserId = customers[0].UserId,
                        OrderDate = DateTime.ParseExact("2021/10/12", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/10/17", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  }
                        }
                    };

                    Order order6 = new Order()
                    {
                        UserId = customers[1].UserId,
                        OrderDate = DateTime.ParseExact("2021/03/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/03/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 2, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 2, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  }
                        }
                    };

                    Order order7 = new Order()
                    {
                        UserId = customers[1].UserId,
                        OrderDate = DateTime.ParseExact("2021/04/09", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/04/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 2, ItemId = staff.ItemId, ItemName = staff.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  }
                        }
                    };

                    Order order8 = new Order()
                    {
                        UserId = customers[1].UserId,
                        OrderDate = DateTime.ParseExact("2021/06/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/06/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  }
                        }
                    };

                    Order order9 = new Order()
                    {
                        UserId = customers[1].UserId,
                        OrderDate = DateTime.ParseExact("2021/08/10", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/17", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  }
                        }
                    };

                    Order order10 = new Order()
                    {
                        UserId = customers[1].UserId,
                        OrderDate = DateTime.ParseExact("2021/11/12", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/11/19", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 2, ItemId = poi.ItemId, ItemName = poi.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  }
                        }
                    };

                    Order order11 = new Order()
                    {
                        UserId = customers[2].UserId,
                        OrderDate = DateTime.ParseExact("2021/03/20", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/03/27", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   }
                        }
                    };

                    Order order12 = new Order()
                    {
                        UserId = customers[2].UserId,
                        OrderDate = DateTime.ParseExact("2021/04/19", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/04/26", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   }
                        }
                    };

                    Order order13 = new Order()
                    {
                        UserId = customers[2].UserId,
                        OrderDate = DateTime.ParseExact("2021/06/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/06/23", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName   },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName   },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1 , ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1 , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },

                        }
                    };

                    Order order14 = new Order()
                    {
                        UserId = customers[2].UserId,
                        OrderDate = DateTime.ParseExact("2021/08/13", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/20", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1 , ItemId = staff.ItemId, ItemName = staff.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName   },

                        }
                    };

                    Order order15 = new Order()
                    {
                        UserId = customers[2].UserId,
                        OrderDate = DateTime.ParseExact("2021/01/01", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/01/08", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1 , ItemId = staff.ItemId, ItemName = staff.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName  },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName   },

                        }
                    };

                    Order order16 = new Order()
                    {
                        UserId = customers[3].UserId,
                        OrderDate = DateTime.ParseExact("2021/04/11", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/04/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName   },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName   },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName   },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName   },

                        }
                    };

                    Order order17 = new Order()
                    {
                        UserId = customers[3].UserId,
                        OrderDate = DateTime.ParseExact("2021/05/17", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/05/22", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 4, ItemId = staff.ItemId, ItemName = staff.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },

                        }
                    };

                    Order order18 = new Order()
                    {
                        UserId = customers[3].UserId,
                        OrderDate = DateTime.ParseExact("2021/06/13", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/06/18", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName  },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 3, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName  },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 2 , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName },

                        }
                    };

                    Order order19 = new Order()
                    {
                        UserId = customers[3].UserId,
                        OrderDate = DateTime.ParseExact("2021/08/02", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2021/08/07", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 3, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 3 , ItemId = poi.ItemId, ItemName = poi.ItemName  },

                        }
                    };

                    Order order20 = new Order()
                    {
                        UserId = customers[3].UserId,
                        OrderDate = DateTime.ParseExact("2020/02/23", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/02/28", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1 , ItemId = poi.ItemId, ItemName = poi.ItemName },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName  },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1 , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName },

                        }
                    };

                    Order order21 = new Order()
                    {
                        UserId = customers[4].UserId,
                        OrderDate = DateTime.ParseExact("2019/03/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/03/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 2, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 4 , ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName  },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1 , ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName   },

                        }
                    };

                    Order order22 = new Order()
                    {
                        UserId = customers[4].UserId,
                        OrderDate = DateTime.ParseExact("2019/06/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/06/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 3, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName    },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName    },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 3 , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName   },

                        }
                    };

                    Order order23 = new Order()
                    {
                        UserId = customers[4].UserId,
                        OrderDate = DateTime.ParseExact("2019/11/22", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/11/29", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 2 , ItemId = staff.ItemId, ItemName = staff.ItemName },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1 , ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 2 , ItemId = poi.ItemId, ItemName = poi.ItemName },

                        }
                    };

                    Order order24 = new Order()
                    {
                        UserId = customers[4].UserId,
                        OrderDate = DateTime.ParseExact("2019/01/01", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/01/08", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 3, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 4, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName   },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName   },
                        }
                    };

                    Order order25 = new Order()
                    {
                        UserId = customers[4].UserId,
                        OrderDate = DateTime.ParseExact("2019/12/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/12/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1 , ItemId = poi.ItemId, ItemName = poi.ItemName  },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1 , ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName  },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1 , ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1 , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },

                        }
                    };

                    Order order26 = new Order()
                    {
                        UserId = customers[5].UserId,
                        OrderDate = DateTime.ParseExact("2020/12/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/12/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 2, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 2, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 3 , ItemId = poi.ItemId, ItemName = poi.ItemName  },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 4, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName   },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 2 , ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1 , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },

                        }
                    };

                    Order order27 = new Order()
                    {
                        UserId = customers[5].UserId,
                        OrderDate = DateTime.ParseExact("2020/03/16", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/03/23", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1 , ItemId = poi.ItemId, ItemName = poi.ItemName },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName  },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1 , ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1 , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName },

                        }
                    };

                    Order order28 = new Order()
                    {
                        UserId = customers[5].UserId,
                        OrderDate = DateTime.ParseExact("2020/01/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/01/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName  },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName  },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName  },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1 , ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },

                        }
                    };

                    Order order29 = new Order()
                    {
                        UserId = customers[5].UserId,
                        OrderDate = DateTime.ParseExact("2020/03/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2020/03/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1 , ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName  },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName   },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1 , ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName  },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1 , ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1  , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName },

                        }
                    };

                    Order order30 = new Order()
                    {
                        UserId = customers[5].UserId,
                        OrderDate = DateTime.ParseExact("2019/12/14", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        ShipmentDate = DateTime.ParseExact("2019/12/21", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                        Items = new List<ItemOrder>()
                        {
                            new ItemOrder(){Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName   },
                            new ItemOrder(){Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName   },
                            new ItemOrder(){Item = poi, Price = poi.Price, Quantity = 1 , ItemId = poi.ItemId, ItemName = poi.ItemName  },
                            new ItemOrder(){Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName   },
                            new ItemOrder(){Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1 , ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName  },
                            new ItemOrder(){Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1 , ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName  },
                        }
                    };



                    initOrders.Add(order1);
                    initOrders.Add(order2);
                    initOrders.Add(order3);
                    initOrders.Add(order4);
                    initOrders.Add(order5);
                    initOrders.Add(order6);
                    initOrders.Add(order7);
                    initOrders.Add(order8);
                    initOrders.Add(order9);
                    initOrders.Add(order10);
                    initOrders.Add(order11);
                    initOrders.Add(order12);
                    initOrders.Add(order13);
                    initOrders.Add(order14);
                    initOrders.Add(order15);
                    initOrders.Add(order16);
                    initOrders.Add(order17);
                    initOrders.Add(order18);
                    initOrders.Add(order19);
                    initOrders.Add(order20);
                    initOrders.Add(order21);
                    initOrders.Add(order21);
                    initOrders.Add(order22);
                    initOrders.Add(order23);
                    initOrders.Add(order24);
                    initOrders.Add(order25);
                    initOrders.Add(order26);
                    initOrders.Add(order27);
                    initOrders.Add(order28);
                    initOrders.Add(order29);
                    initOrders.Add(order30);

                    // final db actions:

                    dbContext.Orders.AddRange(initOrders);// add each order seperatly
                    dbContext.SaveChanges();
                }
            };
        }


    }
}

