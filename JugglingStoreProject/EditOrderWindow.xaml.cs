﻿using JugglingStoreProject.ClassModels;
using JugglingStoreProject.DataAccess;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;


namespace JugglingStoreProject
{
    /// <summary>
    /// Interaction logic for EditOrderWindow.xaml
    /// </summary>
    public partial class EditOrderWindow : Window
    {
        public static List<Item> CurrentItems = new List<Item>();
        public static Order currentOrder;
        public List<User> customers = new CustomerDataAccess().GetAllCustomers();
        public static User currentCustomer;
        public EditOrderWindow(Order currOrder)
        {
            InitializeComponent();

            var loggedInCustomer = LoginDetails.GetInstnace();

            currentOrder = currOrder;

            OrderItemsGrid.ItemsSource =  OrderDataAccess.GetOrderByID(currentOrder.OrderId).Items;
        }

        private void ItemPickedGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Order" || e.PropertyName == "OrderId" || e.PropertyName == "ItemOrderId" || e.PropertyName == "Item")
                e.Column.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void AddStaffToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item staff = dbContext.Items.FirstOrDefault(i => i.ItemName == "staff");

                var orderEdit = dbContext.Orders.Include("Items").FirstOrDefault(x => x.OrderId == currentOrder.OrderId);

                var orderItem = new ItemOrder() { Price = staff.Price, ItemId = staff.ItemId, ItemName = staff.ItemName, OrderId = orderEdit.OrderId };

                if (dbContext.ItemOrder.Contains(orderItem) == true)
                {
                    var item = orderEdit.Items.FirstOrDefault(i => i.ItemId == staff.ItemId);
                    item.Quantity += 1;
                }
                else
                {
                    orderEdit.Items.Add(new ItemOrder() { Item = staff, Price = staff.Price, Quantity = 1, Order = currentOrder, ItemId = staff.ItemId, ItemName = staff.ItemName });

                }
                dbContext.Orders.Update(orderEdit);
                OrderItemsGrid.ItemsSource = orderEdit.Items;
                dbContext.SaveChanges();
            }

            OrderItemsGrid.ItemsSource = OrderDataAccess.GetOrderByID(currentOrder.OrderId).Items;
            CollectionViewSource.GetDefaultView(OrderItemsGrid.ItemsSource).Refresh();
        }

        private void AddBallsToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item jugglingBalls = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling balls");

                var orderEdit = dbContext.Orders.Include("Items").FirstOrDefault(x => x.OrderId == currentOrder.OrderId);

                var orderItem = new ItemOrder() { Price = jugglingBalls.Price, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName, OrderId = orderEdit.OrderId };

                if (dbContext.ItemOrder.Contains(orderItem) == true)
                {
                    var item = orderEdit.Items.FirstOrDefault(i => i.ItemId == jugglingBalls.ItemId);
                    item.Quantity += 1;
                }
                else
                {
                    orderEdit.Items.Add(new ItemOrder() { Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, Order = currentOrder, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName });
                }


                dbContext.Orders.Update(orderEdit);
                OrderItemsGrid.ItemsSource = orderEdit.Items;
                dbContext.SaveChanges();
            }

            CollectionViewSource.GetDefaultView(OrderItemsGrid.ItemsSource).Refresh();
        }

        private void AddPoiToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item poi = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "poi");

                var orderEdit = dbContext.Orders.Include("Items").FirstOrDefault(x => x.OrderId == currentOrder.OrderId);

                var orderItem = new ItemOrder() { Price = poi.Price, ItemId = poi.ItemId, ItemName = poi.ItemName, OrderId = orderEdit.OrderId };

                if (dbContext.ItemOrder.Contains(orderItem) == true)
                {
                    var item = orderEdit.Items.FirstOrDefault(i => i.ItemId == poi.ItemId);
                    item.Quantity += 1;
                }
                else
                {
                    orderEdit.Items.Add(new ItemOrder() { Item = poi, Price = poi.Price, Quantity = 1, Order = currentOrder, ItemId = poi.ItemId, ItemName = poi.ItemName });
                }


                dbContext.Orders.Update(orderEdit);
                OrderItemsGrid.ItemsSource = orderEdit.Items;
                dbContext.SaveChanges();
            }

            CollectionViewSource.GetDefaultView(OrderItemsGrid.ItemsSource).Refresh();
        }

        private void AddFlowerToOrder_Click(object sender, RoutedEventArgs e)
        {
            // this works !!!!!!
            using (var dbContext = new JugglingStoreContext())
            {
                Item flowerSticks = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "flower sticks");

                var orderEdit = dbContext.Orders.Include("Items").FirstOrDefault(x => x.OrderId == currentOrder.OrderId);

                var orderItem = new ItemOrder() { Price = flowerSticks.Price, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName, OrderId = orderEdit.OrderId };

                if (dbContext.ItemOrder.Contains(orderItem) == true)
                {
                    var item = orderEdit.Items.FirstOrDefault(i => i.ItemId == flowerSticks.ItemId);
                    item.Quantity += 1;
                }
                else
                {
                    orderEdit.Items.Add(new ItemOrder() { Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1, Order = currentOrder, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName });
                }


                dbContext.Orders.Update(orderEdit);
                OrderItemsGrid.ItemsSource = orderEdit.Items;
                dbContext.SaveChanges();
            }

            CollectionViewSource.GetDefaultView(OrderItemsGrid.ItemsSource).Refresh();
        }

        private void AddJugglingSwordToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item jugglingSword = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling sword");

                var orderEdit = dbContext.Orders.Include("Items").FirstOrDefault(x => x.OrderId == currentOrder.OrderId);

                var orderItem = new ItemOrder() { Price = jugglingSword.Price, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName, OrderId = orderEdit.OrderId };

                if (dbContext.ItemOrder.Contains(orderItem) == true)
                {
                    var item = orderEdit.Items.FirstOrDefault(i => i.ItemId == jugglingSword.ItemId);
                    item.Quantity += 1;
                }
                else
                {
                    orderEdit.Items.Add(new ItemOrder() { Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, Order = currentOrder, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName });
                }
               

                dbContext.Orders.Update(orderEdit);
                OrderItemsGrid.ItemsSource = orderEdit.Items;
                dbContext.SaveChanges();
            }

            CollectionViewSource.GetDefaultView(OrderItemsGrid.ItemsSource).Refresh();
        }

        private void AddRopeDartToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item ropeDart = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "rope dart");

                var orderEdit = dbContext.Orders.Include("Items").FirstOrDefault(x => x.OrderId == currentOrder.OrderId);

                var orderItem = new ItemOrder() { Price = ropeDart.Price, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName, OrderId = orderEdit.OrderId };

                if (dbContext.ItemOrder.Contains(orderItem) == true)
                {
                    var item = orderEdit.Items.FirstOrDefault(i => i.ItemId == ropeDart.ItemId);
                    item.Quantity += 1;
                }
                else
                {
                    orderEdit.Items.Add(new ItemOrder() { Item = ropeDart, Price = ropeDart.Price, Quantity = 1, Order = currentOrder, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName });
                }


                dbContext.Orders.Update(orderEdit);
                OrderItemsGrid.ItemsSource = orderEdit.Items;
                dbContext.SaveChanges();
            }

            CollectionViewSource.GetDefaultView(OrderItemsGrid.ItemsSource).Refresh();
        }


        private void UpdateOrder_Click_1(object sender, RoutedEventArgs e)
        {
            
            this.Close();
        }

        private void RemoveItemBtn_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                int value;

                if (ItemToRemoveIDTxtBox.Text == "")
                {
                    MessageBox.Show("please enter item id for deletion !");
                }

                else if (int.TryParse(ItemToRemoveIDTxtBox.Text, out value) == false)
                {
                    MessageBox.Show("input id not valid !");
                }

                else
                {
                    var orderEdit = dbContext.Orders.Include("Items").FirstOrDefault(x => x.OrderId == currentOrder.OrderId);

                    ItemOrder item = orderEdit.Items.FirstOrDefault(i => i.ItemId == int.Parse(ItemToRemoveIDTxtBox.Text));

                    if (item == null)
                    {
                        MessageBox.Show("item not found !");
                    }

                    else
                    {
                        bool isRemoved = orderEdit.Items.Remove(item);

                        dbContext.Orders.Update(orderEdit);
                        dbContext.SaveChanges();
                    }    
                }
            }

            OrderItemsGrid.ItemsSource = OrderDataAccess.GetOrderByID(currentOrder.OrderId).Items;
            CollectionViewSource.GetDefaultView(OrderItemsGrid.ItemsSource).Refresh();
        }
    }
}
