﻿using JugglingStoreProject.DataAccess;
using System.Windows;


namespace JugglingStoreProject
{
    /// <summary>
    /// Interaction logic for SignUpWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
        public SignUpWindow()
        {
            InitializeComponent();
        }

        private void SignUpBtn_Click(object sender, RoutedEventArgs e)
        {
            bool isAdded = false;
            if (userNameTxt.Text != "" && PasswordTxt.Password != "" && firstNameTxt.Text != "" && lastNameTxt.Text != "" && addressTxt.Text != "")
            {
                isAdded = new CustomerDataAccess().AddCustomer(userNameTxt.Text, PasswordTxt.Password, firstNameTxt.Text, lastNameTxt.Text, addressTxt.Text, false);
                if (isAdded == true)
                {
                    signUpResult.Text = "Signing up finished successfully";
                    this.Close();
                }
                else
                {
                    signUpResult.Text = "Something went wrong, try again";
                }
            }
            else
            {
                signUpResult.Text = "Something went wrong, try again";
            }

        }
    }
}
