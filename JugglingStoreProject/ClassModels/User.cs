﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace JugglingStoreProject.ClassModels
{
    public class User 
    {
        public User()
        {
            this.Orders = new List<Order>();
        }
        
        //class Props:
        [Key]
        public int UserId { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
        // establishes one to many relationship to orders.
        public virtual ICollection<Order> Orders { get; set; }    
    }

}
