﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using JugglingStoreProject.ClassModels;

namespace JugglingStoreProject
{
     public class Item
    {
        public Item()
        {
            this.Orders = new List<ItemOrder>();
        }
        //class Props:
        [Key]
        public int ItemId { get; set; }
        [Required]
        public string ItemName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public ColorPick Color { get; set; }
        // establish many to many realtionship to orders.
        public virtual ICollection<ItemOrder> Orders { get; set; }
    }

    public enum ColorPick
    {
        Red =1, 
        Black =2,
        White =3,
        Blue =4
    }
}
