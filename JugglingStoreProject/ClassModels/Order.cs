﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using JugglingStoreProject.DataAccess;
using System.ComponentModel;

namespace JugglingStoreProject.ClassModels
{
    public class Order
    {
        public Order()
        {
            this.Items = new List<ItemOrder>();
        }
        [Key]
        public int OrderId { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public DateTime ShipmentDate { get; set; }
        [Required]
        [ForeignKey("Customer_Id")]
        public int UserId { get; set; }
        [Required]
        // establishes many to many relationship to Items.
        public virtual ICollection<ItemOrder> Items { get; set; }
        

        

    }
}
