﻿using JugglingStoreProject.ClassModels;
using JugglingStoreProject.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;


namespace JugglingStoreProject
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public static List<Item> CurrentItems = new List<Item>();
        public static Order currentOrder;
        public List<User> customers = new CustomerDataAccess().GetAllCustomers();
        public static User currentCustomer;
        public Window1()
        {
            InitializeComponent();

            var loggedInCustomer = LoginDetails.GetInstnace();

            currentCustomer = customers.Find(c => c.UserName == loggedInCustomer.UserName && c.Password == loggedInCustomer.Password);

            currentCustomer = new CustomerDataAccess().GetCustomerById(currentCustomer.UserId);

            if (currentCustomer.Orders == null)
            {
                currentCustomer.Orders = new List<Order>();
            }
            currentOrder = new Order();
            currentOrder.Items = new List<ItemOrder>();
            ItemPickedGrid.ItemsSource = currentOrder.Items;


        }


        private void ItemPickedGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Order" || e.PropertyName == "OrderId" || e.PropertyName == "ItemOrderId" || e.PropertyName == "Item")
                e.Column.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void AddStaffToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item staff = dbContext.Items.FirstOrDefault(i => i.ItemName == "staff");
                var Orderitem = new ItemOrder() { Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName };

                if (currentOrder.Items.Any(i => i.ItemId == staff.ItemId))
                {
                    var item = currentOrder.Items.FirstOrDefault(i => i.ItemId == staff.ItemId);
                    item.Quantity += 1;
                }

                else 
                {
                    currentOrder.Items.Add(new ItemOrder() { Item = staff, Price = staff.Price, Quantity = 1, ItemId = staff.ItemId, ItemName = staff.ItemName });
                }
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddBallsToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item jugglingBalls = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling balls");
                var Orderitem = new ItemOrder() { Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName };

                if (currentOrder.Items.Any(i => i.ItemId == jugglingBalls.ItemId))
                {
                    var item = currentOrder.Items.FirstOrDefault(i => i.ItemId == jugglingBalls.ItemId);
                    item.Quantity += 1;
                }

                else
                {
                    currentOrder.Items.Add(new ItemOrder() { Item = jugglingBalls, Price = jugglingBalls.Price, Quantity = 1, ItemId = jugglingBalls.ItemId, ItemName = jugglingBalls.ItemName });
                }
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddPoiToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item poi = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "poi");
                var Orderitem = new ItemOrder() { Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName };

                if (currentOrder.Items.Any(i => i.ItemId == poi.ItemId))
                {
                    var item = currentOrder.Items.FirstOrDefault(i => i.ItemId == poi.ItemId);
                    item.Quantity += 1;
                }

                else
                {
                    currentOrder.Items.Add(new ItemOrder() { Item = poi, Price = poi.Price, Quantity = 1, ItemId = poi.ItemId, ItemName = poi.ItemName });
                }
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddFlowerToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item flowerSticks = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "flower sticks");
                var Orderitem = new ItemOrder() { Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName };

                if (currentOrder.Items.Any(i => i.ItemId == flowerSticks.ItemId))
                {
                    var item = currentOrder.Items.FirstOrDefault(i => i.ItemId == flowerSticks.ItemId);
                    item.Quantity += 1;
                }

                else
                {
                    currentOrder.Items.Add(new ItemOrder() { Item = flowerSticks, Price = flowerSticks.Price, Quantity = 1, ItemId = flowerSticks.ItemId, ItemName = flowerSticks.ItemName });
                }
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddJugglingSwordToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item jugglingSword = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "juggling sword");
                var Orderitem = new ItemOrder() { Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName };

                if (currentOrder.Items.Any(i => i.ItemId == jugglingSword.ItemId))
                {
                    var item = currentOrder.Items.FirstOrDefault(i => i.ItemId == jugglingSword.ItemId);
                    item.Quantity += 1;
                }

                else
                {
                    currentOrder.Items.Add(new ItemOrder() { Item = jugglingSword, Price = jugglingSword.Price, Quantity = 1, ItemId = jugglingSword.ItemId, ItemName = jugglingSword.ItemName });
                }
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void AddRopeDartToOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var dbContext = new JugglingStoreContext())
            {
                Item ropeDart = (Item)dbContext.Items.FirstOrDefault(i => i.ItemName == "rope dart");
                var Orderitem = new ItemOrder() { Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName };

                if (currentOrder.Items.Any(i => i.ItemId == ropeDart.ItemId))
                {
                    var item = currentOrder.Items.FirstOrDefault(i => i.ItemId == ropeDart.ItemId);
                    item.Quantity += 1;
                }

                else
                {
                    currentOrder.Items.Add(new ItemOrder() { Item = ropeDart, Price = ropeDart.Price, Quantity = 1, ItemId = ropeDart.ItemId, ItemName = ropeDart.ItemName });
                }
            }

            CollectionViewSource.GetDefaultView(ItemPickedGrid.ItemsSource).Refresh();
        }

        private void SendOrder_Click(object sender, RoutedEventArgs e)
        {

            if (currentCustomer.Orders == null)
            {
                currentCustomer.Orders = new List<Order>();
            }


            using (var dbContext = new JugglingStoreContext())
            {
                currentOrder.UserId = currentCustomer.UserId;
                currentOrder.OrderDate = DateTime.Now;
                

                if (currentCustomer.IsAdmin == true)
                {
                    currentOrder.ShipmentDate = DateTime.Now.AddDays(5); // shorter delivery time for admins.
                }
                else
                {
                    currentOrder.ShipmentDate = DateTime.Now.AddDays(7);
                }

                currentOrder.Items = (ICollection<ItemOrder>)ItemPickedGrid.ItemsSource;

                currentCustomer = new CustomerDataAccess().GetCustomerById(currentCustomer.UserId);


                currentCustomer.Orders.Add(currentOrder);

                dbContext.Users.Update(currentCustomer);
                dbContext.Orders.Add(currentOrder);
                dbContext.SaveChanges();
            }

            this.Close();
        }

        private void quitMain_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

