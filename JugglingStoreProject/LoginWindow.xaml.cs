﻿using JugglingStoreProject.ClassModels;
using JugglingStoreProject.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Windows;

namespace JugglingStoreProject
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            // popualte DB: check if this works...

            bool isPopulated = false;

            using (var dbContext = new JugglingStoreContext())
            {
                dbContext.Database.Migrate();

                if (dbContext.Orders.Count() == 0 && dbContext.Users.Count() == 0 && dbContext.Items.Count() == 0)
                {
                    isPopulated = false;
                }
                else
                {
                    isPopulated = true;
                }
            }

            if (isPopulated == false)
            {
                new CustomerDataAccess().PopulateCustomerTable();
                new ItemDataAccess().PopulateItemTable();
                OrderDataAccess.PopulateOrderTable();
            }

            InitializeComponent();
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            var customers = new CustomerDataAccess().GetAllCustomers();
            try
            {
                var customer = from c in customers
                               where (c.UserName == userNameTxt.Text) && (c.Password == PasswordTxt.Password.ToString())
                               select c as User;

                if (VerifyLoginDetails(userNameTxt.Text, PasswordTxt.Password.ToString()))
                {
                    //singelton:
                    LoginDetails loginDetails = LoginDetails.GetInstnace();
                    loginDetails.UserName = userNameTxt.Text;
                    loginDetails.Password = PasswordTxt.Password.ToString();

                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                    this.Close();
                }
            }
            catch (Exception ex)
            {

            }

        }

        //Method for checking Log in Details by user
        public bool VerifyLoginDetails(string userName, string Password)
        {
            bool verified = false;
            var customers = new CustomerDataAccess().GetAllCustomers();

            for (int i = 0; i < customers.Count; i++)
            {
                if (userName == customers[i].UserName && Password == customers[i].Password)
                {
                    verified = true;
                    //currentCustomer = new CustomerDataAccess().GetCustomerById(customers[i].CustomerId);
                    LoginResultBox.Text = "Login Successfull !!!";
                    break;
                }
                else
                {
                    verified = false;
                    LoginResultBox.Text = "Login failed...";
                }
            }
            return verified;
        }

        private void SignUpBtn_Click(object sender, RoutedEventArgs e)
        {
            SignUpWindow signUpWindow = new SignUpWindow();
            signUpWindow.Show();
        }
    }
}
