# JugglingStoreProject
**About the project:**

This project simulates an online store for displaying and selling juggling products. It was built to show knowledge and technical control in C#, Entity Framework and SQL.

**Technologies:**

The project was Built using the communication library &quot; **Entity Framework Core**&quot; ( **V 5.0.11** ) to access the database, and &quot; **Microsoft SQL Server**&quot; as the database management system. in addition, the GUI for this program was built with WPF graphical environment.

The project implements the &quot; **Singelton**&quot; design pattern.

**Set-Up:**

Download the code source:

Download or clone the Git source code from: [https://gitlab.com/Ido.navot/jugglingstoreprojectupdatedLast)

Build And Run solution. The Login screen should appear to allow you to log in.

**Admin user** :

User Name: IdoNavot303

Password: 12312

**Regular User** :

User Name: Dandi88

Password: 972310

**Database - setup:**

When loaded, the program invokes a method to checks if the database exists. if the database does not exist, this mentioned method automatically creates and populates the database.

**Entities, Relations design and logic:**

The project includes 3 entities (Classes): Customer, Item, Order.

Customer – Order: one to many relationship.

Order – item: many to many relationship.

There is a table for each entity to hold the data, and a joining table that is not visible in the code that has been generated using EF to establish and implement a many to many relationship between Item entity and Order Entity.

![](SystemImages/1.PNG)

**Structure and Functionality:**

The DataAccess folder contains different data access classes, which offer various data operations methods to be executed.

Following is a key list of these methods:

Admin only methods:

- Retrieve all orders in the system.
- View total customers number.
- View total orders number.
- Total order price is 20% off.
- Order delivery time is only 5 days.

Regular customer methods:

- Retrieve all personal orders in the system.
- Retrieve all personal orders between chosen dates in the system.
- View order total price.
- View order items.
- Order delivery time is 7 days.
- Make and add a new order.
- Sign up as a new customer.
- Login method.

**Premises:**

- This project does not manages inventory – There are infinite Items so there can be no case of an item running out of the store inventory.
- This project does not allow all CRUD operations on orders due to business logic.

**GUI screens:**

Log-in screen:Sign-up screen:

![](SystemImages/2.PNG) ![](SystemImages/3.PNG)

Admin main screen:Regular customer main screen:

![](SystemImages/4.PNG) ![](SystemImages/5.PNG)

New order screen:
![](SystemImages/6.PNG)

